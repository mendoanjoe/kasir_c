# **Kasir C**
kasir c adalah aplikasi simple yang dibuat dengan menggunakan bahasa c++. Database menggunakan file dengan nama read.txt format `kodebarang,namabarang,hargabarang,stok`. Aplikasi ini masih terdapat kekurangan, silahkan berkontribusi.

## Prosedur
adalah modul program yang mengerjakan tugas/aktifitas yang spesifik dan menghasilkan suatu efek netto.
 1. [`void cetakJudul(string judul);`](#cetakjudul)
 2. [`void bacaFile();`](#bacafile)
 3. [`void simpanFile();`](#simpanfile)
 4. [`void cetakBarang();`](#cetakbarang)

## Fungsi
adalah modul program yang mengerjakan tugas/aktifitas yang spesifik dan menghasilkan kembalian kepada pemanggil.
 1. [`mencariBarang(string kodebarang);`](#mencaribarang)
 2. [`kodeBarangTerakhir();`](#kodebarangterakhir)

## Template
adalah modul program yang mengerjakan tugas/aktifitas dengan konstruksi kode yang menyesuaikan dengan keinginan si pemanggil.
 1. [`fromString<tipedata>(s);`](#fromstring)
 2. [`toString(data apapun)`](#tostring)

## Todo - list
 - [x] if-else
 - [x] switch-case
 - [x] for
 - [x] do-while
 - [x] array 2d
 - [x] prosedur
 - [x] fungsi
 - [ ] algoritma rekursif - memanggil dirinya sendiri
 - [x] algoritma pencarian

## Menu - list
 - [x] Stok Barang
 - [x] Tambah Barang
 - [x] Hapus Barang
 - [x] Edit Barang
 - [x] Pembelian
 - [x] Keluar