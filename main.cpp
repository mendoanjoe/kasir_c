#include <iostream> // library untuk input output
#include <sstream> // library untuk stream data
#include <fstream> // library untuk membuka file
#include <vector> // library untuk array vector

using namespace std;

/*
* PROSEDUR
* cetakBarang = digunakan untuk mencetak daftar barang
* cetakJudul @judul = digunakan untuk mencetak penulisan judul
* simpanFile = digunakan untuk menyimpan items ke file read.txt
* bacaFile = digunakan untuk membaca read.txt dan memberi nilai pada items
*/
void cetakBarang();
void cetakJudul(string judul);
void simpanFile();
void bacaFile();

/*
* FUNGSI
* mencariBarang @kodebarang = digunakan untuk mencari barang di items berdasarkan kode barang
* kodeBarangTerakhir = digunakan untuk mencari kodebarang terakhir di dalam file
*/
int mencariBarang(string kodebarang);
int kodeBarangTerakhir();

/*
* VARIABLE PUBLIK
* dimana dapat di edit dan di akses dimana saja
* menggunakan array vector 2D
*/
vector<vector<string> > items;

/*
* KONVERSI
* konversi dari tipe data string ke tipe data lainnya
* http://www.cplusplus.com/forum/general/208135/
*/
template <class T> T fromString(string s){
    T value;
    stringstream ss(s);
    ss >> value;
    return value;
}

/*
* KONVERSI
* konversi dari tipe data lainnya ke tipe data string
* http://www.cplusplus.com/forum/general/208135/
*/
template <class T> string toString(T value){
    stringstream ss;
    ss << value;
    return ss.str();
}

/*
* PROGRAM UTAMA
*/
int main()
{
    /*
    * MEMBACA FILE
    * membaca file read.txt dan memberi nilai pada items
    */
    bacaFile();

    /*
    * PENGULANGAN MENU
    * dikarenakan do while akan melakukan pengecheckan
    * setelah mengeksekusi statement
    */
    int pilihanMenu;
    do {
        cetakJudul("Aplikasi Kasir");
        cout << "1. Stok Barang" << endl;
        cout << "2. Tambah Barang" << endl;
        cout << "3. Hapus Barang" << endl;
        cout << "4. Edit Barang" << endl;
        cout << "5. Pembelian" << endl;
        cout << "6. Keluar" << endl;
        cout << "===============================" << endl;
        cout << endl << "Masukan Pilihan Menu : "; cin >> pilihanMenu;
        cout << endl;

        /*
        * mengecheck apakah masukan dari pengguna
        * berupa angka
        */
        if(cin.fail()){
            cout << endl << "Masukan angka" << endl;
            return 0;
        }

        switch(pilihanMenu){
            case 1:{
                /*
                * memperbarui data pada items dengan
                * memanggil bacafile
                */
                bacaFile();

                /*
                * mencetak daftar barang dari items
                * yang ada di variable publik
                */
                cetakBarang();

                break;
            }
            case 2:{
                /*
                * menambah barang
                * dilakukan dengan cara menambahkan data yang dimasukan ke dalam items di variable publik
                * kemudian akan memanggil prosedur simpanfile untuk menyimpannya kedalam read.txt
                */
                int jumlahBarang;

                cetakJudul("Tambah Barang");
                cout << "Berapa barang yang ingin di tambahkan : "; cin >> jumlahBarang;

                /*
                * mengecheck apakah masukan dari pengguna
                * berupa angka
                */
                if(cin.fail()){
                    cout << endl << "Masukan angka" << endl;
                    return 0;
                }

                /*
                * untuk menggunakan getline maka harus memanggil cin.ignore()
                * getline digunakan untuk mengambil semua karakter termasuk spasi pada inputan
                */
                cin.ignore();

                /*
                * pengulangan dilakukan sesuai dengan
                * banyaknya barang yang akan disimpan
                */
                string arrayData[jumlahBarang][4];
                for(int a = 0; a < jumlahBarang; a++){
                    /*
                    * menggunakan kodebarang dari
                    * memanggil fungsi, untuk mencegah dulikasi kodebarang
                    */
                    arrayData[a][0] = toString(kodeBarangTerakhir() + 1);

                    cout << "Masukan nama barang, ke-" << (a+1) << " : "; getline(cin, arrayData[a][1]);
                    cout << "Masukan harga barang, ke-" << (a+1) << " : "; getline(cin, arrayData[a][2]);
                    cout << "Masukan stok barang, ke-" << (a+1) << " : "; getline(cin, arrayData[a][3]);
                    cout << endl;

                    /*
                    * menampung data untuk sementara
                    * yang kemudian akan di kirimkan ke items
                    */
                    vector<string> tempVector;
                    for(int d = 0; d < 4; d++){
                        tempVector.push_back(arrayData[a][d]);
                    }

                    items.push_back(tempVector);
                    tempVector.clear();
                }
                simpanFile();

                /*
                * mencetak barang berhasil di tambahkan
                */
                cout << endl << "Barang berhasil ditambahkan" << endl;

                break;
            }
            case 3:{
                /*
                * menghapus barang
                * memperbarui items di variable publik
                * dan juga memperbarui file read.txt
                */
                cetakJudul("Menghapus Barang");

                /*
                * mencetak daftar barang dari items
                * yang ada di variable publik
                */
                cetakBarang();

                string kodeBarang;
                cout << "Masukan kode barang : "; cin >> kodeBarang;

                /*
                * mengecheck apakah masukan dari pengguna
                * berupa angka
                */
                if(cin.fail()){
                    cout << endl << "Masukan angka" << endl;
                    return 0;
                }

                /*
                * memanggil fungsi pencarian barang berdasarkan kode barang
                * dengan keluaran berupa index row pada items di variable publik
                */
                int hasilMencari = mencariBarang(kodeBarang);
                if(hasilMencari != -1){
                    string pilihan;
                    cout << "Yakin ingin menghapus " << kodeBarang << " ? (ya/tidak) "; cin >> pilihan;

                    if(pilihan == "ya" || pilihan == "YA"){
                        /*
                        * menghapus data yang terdapat pada items di variable publik
                        * dan menyimpan perubahan data ke file read.txt
                        */
                        items.erase(items.begin() + hasilMencari);
                        simpanFile();

                        /*
                        * mencetak barang berhasil di hapus
                        */
                        cout << endl << "Barang berhasil dihapus" << endl;
                    }else{
                        cout << endl << "Barang tidak dihapus" << endl;
                    }
                }else{
                    cout << "Kode barang tidak ditemukan" << endl;
                }

                break;
            }
            case 4:{
                /*
                * mengedit barang
                * memperbarui items di variable publik
                * dan juga memperbarui file read.txt
                */
                cetakJudul("Mengedit Barang");

                /*
                * mencetak daftar barang dari items
                * yang ada di variable publik
                */
                cetakBarang();

                string kodeBarang;
                cout << "Masukan kode barang : "; cin >> kodeBarang;

                /*
                * mengecheck apakah masukan dari pengguna
                * berupa angka
                */
                if(cin.fail()){
                    cout << endl << "Masukan angka" << endl;
                    return 0;
                }

                /*
                * memanggil fungsi pencarian barang berdasarkan kode barang
                * dengan keluaran berupa index row pada items di variable publik
                */
                int hasilMencari = mencariBarang(kodeBarang);
                if(hasilMencari != -1){
                    string arrayData[4];

                    for(int a = 0; a < 4; a++){
                        arrayData[a] = items[hasilMencari][a];
                    }

                    /*
                    * untuk menggunakan getline maka harus memanggil cin.ignore()
                    * getline digunakan untuk mengambil semua karakter termasuk spasi pada inputan
                    */
                    cin.ignore();

                    cout << "Edit nama barang, (" << arrayData[1] << ") : "; getline(cin, arrayData[1]);
                    cout << "Edit harga barang, (" << arrayData[2] << ") : "; getline(cin, arrayData[2]);
                    cout << "Edit stok barang, (" << arrayData[3] << ") : "; getline(cin, arrayData[3]);
                    cout << endl;

                    string pilihan;
                    cout << "Yakin ingin menyimpan hasil edit" << kodeBarang << " ? (ya/tidak) "; cin >> pilihan;

                    if(pilihan == "ya" || pilihan == "YA"){
                        for(int a = 0; a < 4; a++){
                            items[hasilMencari][a] = arrayData[a];
                        }

                        simpanFile();
                        cout << endl << "Barang berhasil diedit" << endl;
                    }else{
                        cout << endl << "Barang tidak diedit" << endl;
                    }
                }else{
                    cout << "Kode barang tidak ditemukan" << endl;
                }

                break;
            }
            case 5:{
                /*
                * pembelian barang
                * total pembelian berdasarkan kode barang
                * dimana total pembelian adalah jumlah barang * harga barang
                */
                cetakJudul("Pembelian Barang");

                /*
                * mencetak daftar barang dari items
                * yang ada di variable publik
                */
                cetakBarang();

                string kodeBarang;
                cout << "Masukan kode barang : "; cin >> kodeBarang;

                /*
                * mengecheck apakah masukan dari pengguna
                * berupa angka
                */
                if(cin.fail()){
                    cout << endl << "Masukan angka" << endl;
                    return 0;
                }

                /*
                * memanggil fungsi pencarian barang berdasarkan kode barang
                * dengan keluaran berupa index row pada items di variable publik
                */
                int hasilMencari = mencariBarang(kodeBarang);
                if(hasilMencari != -1){
                    string arrayData[4];

                    for(int a = 0; a < 4; a++){
                        arrayData[a] = items[hasilMencari][a];
                    }

                    int totalItem;
                    cout << "Total item belanja : "; cin >> totalItem;

                    /* check stok */
                    if(totalItem <= fromString<int>(items[hasilMencari][3])){
                        cout << "Total belanja : Rp." << totalItem * fromString<int>(items[hasilMencari][2]) << endl;

                        arrayData[3] = toString(fromString<int>(items[hasilMencari][3]) - totalItem);

                        for(int a = 0; a < 4; a++){
                            items[hasilMencari][a] = arrayData[a];
                        }

                        simpanFile();
                        cout << endl << "Pembelian berhasil" << endl;
                    }else{
                        cout << "Stok barang tidak mencukupi total item belanja, silakan kurangi belanjaan" << endl;
                    }
                }else{
                    cout << "Kode barang tidak ditemukan" << endl;
                }

                break;
            }
            case 6:{
                /*
                * menghentikan program
                * menggunakan return 0
                */
                return 0;

                break;
            }
        }
    }while(pilihanMenu < 7 && pilihanMenu > 0);
}

/*
* untuk mencegah duplikat
* kode barang yang ada, dengan logika
* kodebarang terakhir di tambah 1
*/
int kodeBarangTerakhir(){
    /*
    * memerbarui daftar barang yang
    * terdapat pada items
    */
    bacaFile();

    int hasil;
    if(items.size() == 0){
        hasil = 1;
    }else{
        hasil = fromString<int>(items[items.size() - 1][0]);
    }
    return hasil;
}

/*
* untuk mencari dari data yang terdapat
* pada items dengan menggunakan kodebarang
* dengan keluaran berupa index row items
*/
int mencariBarang(string kodebarang){
    /*
    * memerbarui daftar barang yang
    * terdapat pada items
    */
    bacaFile();

    int hasil = -1;
    for (int i = 0; i < items.size(); i++){
        if(items[i][0] == kodebarang){
            hasil = i;
            break;
        }
    }
    return hasil;
}

void simpanFile(){
    /*
    * inisialisasi file dan membuka file read.txt
    * atau apabila tidak ada, maka akan membuat file read.txt
    */
    fstream file;
    file.open("read.txt", fstream::out);

    /*
    * mengambil data yang terdapat pada items
    * dan menyimpan dalam format kodebarang,namabarang,hargabarang,stokbarang
    * pada file read.txt
    */
    for(int a = 0; a < items.size() ; a++){
        for(int b = 0; b < items[a].size(); b++){
            file << items[a][b];
            if(b != 3){
                file << ",";
            }
        }
        if(a < items.size() - 1){
            file << "\n";
        }
    }
    file.close();
}

/*
* mengambil data dari file read.txt
* dan memberi nilai pada items di varaible publik
*/
void bacaFile(){
    /*
    * inisialisasi file dan membuka file read.txt
    */
    fstream file;
    file.open("read.txt");

    string line;

    /*
    * menghapus data pada items
    * untuk mencegah terjadinya data yang sama
    */
    items.clear();
    if(file.is_open()){
        /*
        * pengulangan row, pada file read.txt
        */
        while(getline(file, line, '\n')){
            istringstream templine(line);
            string data;

            /*
            * mengambil data dari setiap baris yang
            * di pisahkan oleh delimiter ','
            */
            vector<string> tempVector;
            while (getline(templine, data, ',')){
                /*
                * menyimpan data ke dalam items di variable publik
                */
                tempVector.push_back(data);
            }
            items.push_back(tempVector);
            tempVector.clear();
        }
        file.close();
    }else{
        /*
        * membuat file read.txt
        */
        file.open("read.txt", fstream::out);
        file.close();
    }
}

/*
* prosedur untuk mencetak judul
* untuk mengurangi pengulangan penulisan kode
*/
void cetakJudul(string judul){
    cout << "===============================" << endl;
    cout << judul << endl;
    cout << "===============================" << endl;
}

/*
* prosedur untuk mencetak daftar barang
* untuk mengurangi pengulangan penulisan kode
*/
void cetakBarang(){
    cetakJudul("STOK BARANG :  \nKode - Nama Barang - Harga - Stok");

    for (int i = 0;i < items.size(); i++){
        for (int j = 0;j < items[i].size(); j++){
            cout << items[i][j];
            if(j != 3){
                cout << " - ";
            }
        }
        if(i < items.size() - 1){
            cout << endl;
        }
    }
    cout << endl;
}
